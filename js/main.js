/*------------------------------------------------------------------------------
JS Document

project:    KMFMUSIC
created:    2019-02-13
author:     Hugo MARTIN

summary:    ---- WINDOW.ONLOAD ----
COOKIES
----------------------------------------------------------------------------- */

/*  =PRE.WINDOW.ONLOAD
----------------------------------------------------------------------------- */
asyncRefresh = function(){
  // liste li
  $.ajax({
    url: 'liauto.php',
    type: 'POST'
  })
  .done(function(data) {
    $('.ulgenre').html(data);
    //----------------
    //Enleve les duplicata de li donnés par le php
    var liText = '', liList = $('.ulgenre li'), listForRemove = [];

    $(liList).each(function () {

      var text = $(this).text();

      if (liText.indexOf('|'+ text + '|') == -1)
        liText += '|'+ text + '|';
      else
        listForRemove.push($(this));

    });
    $(listForRemove).each(function () { $(this).remove(); });
    console.log("success");
  })
  .fail(function() {
    console.log("error");
  });
  // plaque de musique - genere les plaques de musique en asynchrone
  $.ajax({
    type: 'POST',
    url: 'musictemp.php'
  })
  .done(function(data) {
    $('#wrapperMain').html(data);
    $('.dots').click(function(){
      var that = this;
      $('#nomart').val($(that).siblings('.minfo').find('.martist').text());
      $("#nomimg").val($(that).siblings('.imgSec').find('#mimg').val());
      $("#titrechanson").val($(that).siblings('.minfo').find('.mtitle').find('.titre').text());
      $("#anneesort").val($(that).siblings('.minfo').find('.mtitle').find('.date').text());
      $("#genre1").val($(that).siblings('.minfo').find('.desc1').text());
      $("#genre2").val($(that).siblings('.minfo').find('.desc2').text());
      $("#idmusic").val($(that).siblings('#mid').val());
      $("#ident").val($(that).siblings('#mentity').val());
      $("#parent").val($(that).siblings('#mparent').val());
      $('.wrapperModifSupr').removeClass('activate2');
    });

    if ($('.typeU').html() == "admin") {
      $('.register').show();
      $('.addMusic').show();
      $('.dots').show();
    }else if($('.typeU').html() == "user") {
      $('.register').hide();
      $('.dots').show();
    }else {
      $('.register').hide();
      $('.addMusic').hide();
      $('.dots').hide();
    }
    console.log("success");
  })
  .fail(function() {
    console.log("error");
  });
}
asyncRefresh();
/*  =WINDOW.ONLOAD
----------------------------------------------------------------------------- */
$(window).ready(function(){
  // Call Functions
  log();
});
/*  = Login/logout
----------------------------------------------------------------------------- */
log = function(){

  // si le nom apparais alors le bouton change est deviens un bouton de deconnexion
  if ($('.nomU').is(':empty')) {
    $('.login').show();
  }else {
    $('.base').removeClass('login').addClass('logout').html("Déconnexion");
  }
  //animation des formulaires
  $('.register').click(function(event) {
    $('.wrapperRegister').removeClass('activate');
    $('.wrapperAdd').addClass('activate2');
  });
  $('.addMusic').click(function(event) {
    $('.wrapperAdd').removeClass('activate2');
    $('.wrapperRegister').addClass('activate');
  });
  $('.close').click(function(event) {
    $('.wrapperLogin, .wrapperRegister').addClass('activate');
    $('.wrapperAdd, .wrapperModifSupr').addClass('activate2');
  });
  // destruction de la session
  $('.base').click(function(event) {
    if ($(this).html() != "Déconnexion") {
      $('.wrapperLogin').removeClass('activate');
      $('.wrapperRegister').addClass('activate');
    }else{
      window.location.replace("logout.php");
    }
  });

  //montre ou cache des boutons selon le type de l'utilsateur
  if ($('.typeU').html() == "admin") {
    $('.register').show();
    $('.addMusic').show();
    $('.dots').show();
  }else if($('.typeU').html() == "user") {
    $('.register').hide();
    $('.dots').show();
  }else {
    $('.register').hide();
    $('.addMusic').hide();
    $('.dots').hide();
  }
  //en repsonsive affiche un burger menu clickable
  $('.burger').click(function(event) {
    $('nav').addClass('sliding');
    $('.closenav').show();
  });
  $('.closenav').click(function(event) {
    $('nav').removeClass('sliding');
    $('.closenav').hide();
  });
  console.log($('.typeU').html());

}
/*  =
----------------------------------------------------------------------------- */
/*  =
----------------------------------------------------------------------------- */
/*  =
----------------------------------------------------------------------------- */
