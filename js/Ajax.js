$(function() {
  // fonction permettant de recharger la page
  // en asynchrone
  function refreshPage() {
    $.ajax({
      url: 'liauto.php',
      type: 'POST'
    })
    .done(function(data) {
      $('.ulgenre').html(data);
      //----------------
      //Eneleve des duplicata des li générés par le php
      var liText = '', liList = $('.ulgenre li'), listForRemove = [];

      $(liList).each(function () {

        var text = $(this).text();

        if (liText.indexOf('|'+ text + '|') == -1)
          liText += '|'+ text + '|';
        else
          listForRemove.push($(this));

      });
      $(listForRemove).each(function () { $(this).remove(); });
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    });
    //genere les plaque de musique en asynchrone
    $.ajax({
      type: 'POST',
      url: 'musictemp.php'
    })
    .done(function(data) {
      $('#wrapperMain').html(data);
      $('.dots').click(function(){
        var that = this;
        $('#nomart').val($(that).siblings('.minfo').find('.martist').text());
        $("#nomimg").val($(that).siblings('.imgSec').find('#mimg').val());
        $("#titrechanson").val($(that).siblings('.minfo').find('.mtitle').find('.titre').text());
        $("#anneesort").val($(that).siblings('.minfo').find('.mtitle').find('.date').text());
        $("#genre1").val($(that).siblings('.minfo').find('.desc1').text());
        $("#genre2").val($(that).siblings('.minfo').find('.desc2').text());
        $("#idmusic").val($(that).siblings('#mid').val());
        $("#ident").val($(that).siblings('#mentity').val());
        $("#parent").val($(that).siblings('#mparent').val());
        $('.wrapperModifSupr').removeClass('activate2');
      });

      if ($('.typeU').html() == "admin") {
        $('.register').show();
        $('.addMusic').show();
        $('.dots').show();
      }else if($('.typeU').html() == "user") {
        $('.register').hide();
        $('.addMusic').show();
        $('.dots').show();
      }else {
        $('.register').hide();
        $('.addMusic').hide();
        $('.dots').hide();
      }
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    });
  }
  // permet de soumettre le formulaire de connexion
  // en asynchrone
  $('.formLog').submit(function(event) {
    $.ajax({
      type: 'POST',
      url: $(this).attr('action'),
      data: $(this).serialize()
    })
    .done(function(data) {
      var obj = jQuery.parseJSON(data);
      if (obj.status == 'error') {
        alert("ce compte n'existe pas");
        refreshPage();
        $('.wrapperLogin').addClass('activate');
        $("input[name='uname'],input[name='passwd']").val("");
      }else {
        $('.nomU').html(obj.uname);
        $('.typeU').html(obj.type);
        if ($('.nomU').is(':empty')) {
          $('.login').show();
        }else {
          $('.base').removeClass('login').addClass('logout').html("Déconnexion");
        }
        refreshPage();
        $('.wrapperLogin').addClass('activate');
        console.log("success");
      }
    })
    .fail(function() {
      console.log("error");
    });
    return false;
  });
  // permet de soumettre le formulaire d'ajout de musique
  // en asynchrone
  $('.formAdd').submit(function(event) {
    $.ajax({
      type: 'POST',
      url: $(this).attr('action'),
      data: $(this).serialize()
    })
    .done(function() {
      $('.wrapperAdd').addClass('activate2');
      refreshPage();
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    });
    return false;
  });
  // permet de soumettre le formulaire d'ajout d'utilisateur
  // en asynchrone
  $('.formReg').submit(function(event) {
    if ($('.type option:selected').val() == "null") {
      $('.alert').html("Veuillez selectionner un type");
      $('.alert').show();
    }else {
      $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serialize()
      })
      .done(function() {
        $('.wrapperRegister').addClass('activate');
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      });
      return false;
    }
  });
  // permet de soumettre la modification ou la suppression de musique
  // en asynchrone
  $('input[name="Supp"]').click(function(event) {
    $('.formModifSupr').submit(function(event) {
      $.ajax({
        type: 'POST',
        url: 'suppmusic.php',
        data: $(this).serialize()
      })
      .done(function() {
        $('.wrapperModifSupr').addClass('activate2');
        refreshPage();
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      });
      return false;
    });
  });
  //Sur le formulaire de modification /suppression permet de rediriger sur le bon
  //php en fonction du bouton clické
  $('input[name="Modif"]').click(function(event) {
    $('.formModifSupr').submit(function(event) {
      $.ajax({
        type: 'POST',
        url: 'modifmusic.php',
        data: $(this).serialize()
      })
      .done(function() {
        $('.wrapperModifSupr').addClass('activate2');
        refreshPage();
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      });
      return false;
    });
  });
});
