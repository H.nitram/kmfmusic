<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/main.css">
  <title>Kmfmusic</title>
</head>
<body>
  <nav>
    <h1 id="kmf">KMFMusic(<?php
      if(isset($_SESSION['uname']) && !empty($_SESSION['uname'])) {
        echo '<span class="nomU">'.$_SESSION['uname'].'</span>';
        echo '<pre class="typeU">'.$_SESSION['type'].'</pre>';
      }else {
        echo '<span class="nomU"></span>';
        echo '<pre class="typeU"></pre>';
      }
      ?>)
      <span class="closenav">X</span>
    </h1>
    <div class="wrapperNav">
      <ul class="ulgenre">
      </ul>
      <hr>
      <ul>
        <li class="liTitle">ANNÉES</li>
        <li>Années 70</li>
        <li>Années 80</li>
        <li>Années 90</li>
        <li>Années 2000</li>
      </ul>
    </div>
  </nav>
  <main>
    <header>
      <div class="burger">
        <img src="img/burgermenu.svg" alt="burger menu">
      </div>
      <form class="search" action="#" method="post">
        <input type="text" name="search" value="" placeholder="search">
      </form>
      <div class="btnGrp">
        <a href="#" class="base login">Connexion</a>
        <a href="#" class="register">+ User</a>
        <a href="#" class="addMusic">+ Music</a>
      </div>
    </header>
    <section id="wrapperMain">
      <div class="musicase">
        <section class="imgSec">
          <img src="" alt="">
        </section>
        <section>
          <h6 class="mtitle"></h6>
          <p class="mdesc"></p>
        </section>
      </div>
    </section>
  </main>
  <div class="wrapperLogin activate">
    <span class="close">X</span>
    <form class="formLog" action="login.php" method="post">
      <h2>Connectez-vous !</h2>
      <input type="text" name="uname" value="" placeholder="Nom d'utilisateur" required>
      <input type="password" name="passwd" value="" placeholder="Mot de passe" required>
      <input type="submit" name="submit" value="Connexion">
    </form>
  </div>
  <div class="wrapperRegister activate">
    <span class="close">X</span>
    <form class="formReg" action="user.php" method="post">
      <h2>Enregistrer un utilisateur</h2>
      <input type="text" name="uname" value="" placeholder="Nom d'utilisateur" required>
      <input type="password" name="passwd" value="" placeholder="Mot de passe" required>
      <input type="hidden" name="hidden" value="">
      <select class="type" name="type">
        <option value="null" selected disabled>Choix type</option>
        <div class="alert"></div>
        <option value="user">User</option>
        <option value="admin">Admin</option>
      </select>
      <input type="submit" name="submit" value="Enregistrer">
    </form>
  </div>
  <div class="wrapperAdd activate2">
    <span class="close">X</span>
    <form class="formAdd" action="add.php" method="post">
      <h2>Ajoutez une musique</h2>
      <input type="text" name="artname" value="" placeholder="Nom de l'artiste" required>
      <input type="text" name="identity" value="" placeholder="Id de l'entité" required>
      <input type="text" name="genre1" value="" placeholder="Genre" required>
      <input type="text" name="genre2" value="" placeholder="Genre2" required>
      <input type="text" name="image" value="" placeholder="Nom de l'image" required>
      <input type="text" name="parent" value="" placeholder="Parent(en general le même que l'artiste)" required>
      <input type="text" name="date" value="" placeholder="Année de sortie" required>
      <input type="text" name="titre" value="" placeholder="Titre de la chanson" required>
      <input type="submit" name="submit" value="Ajouter">
    </form>
  </div>
  <div class="wrapperModifSupr activate2">
    <span class="close">X</span>
    <form class="formModifSupr" action="" method="post">
      <h2>Modifier/Supprimer une musique</h2>
      <input id="idmusic" type="hidden" name="id" value="">
      <input id="nomart" type="text" name="artname" value="" placeholder="Nom de l'artiste" required>
      <input id="ident" type="text" name="identity" value="" placeholder="Id de l'entité" required>
      <input id="genre1" type="text" name="genre1" value="" placeholder="Genre" required>
      <input id="genre2" type="text" name="genre2" value="" placeholder="Genre2" required>
      <input id="nomimg" type="text" name="image" value="" placeholder="Nom de l'image" required>
      <input id="parent" type="text" name="parent" value="" placeholder="Parent(en general le même que l'artiste)" required>
      <input id="anneesort" type="text" name="date" value="" placeholder="Année de sortie" required>
      <input id="titrechanson" type="text" name="titre" value="" placeholder="Titre de la chanson" required>
      <div class="containerSub">
        <input type="submit" name="Modif" value="Modifier">
        <input type="submit" name="Supp" value="Supprimer">
      </div>
    </form>
  </div>
  <script src="js/jquery-3.3.1.min.js" charset="utf-8"></script>
  <script src="js/main.js" charset="utf-8"></script>
  <script src="js/Ajax.js" charset="utf-8"></script>
</body>
</html>
