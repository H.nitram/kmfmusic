<?php
//trouve et détruit la session et redirige à l'accueil
  session_start();
  session_destroy();
  header('Location: index.php');
?>
