-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 26 fév. 2019 à 20:04
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sfhmartin`
--
CREATE DATABASE IF NOT EXISTS `sfhmartin` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sfhmartin`;

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190212101015', '2019-02-12 10:10:44');

-- --------------------------------------------------------

--
-- Structure de la table `music`
--

DROP TABLE IF EXISTS `music`;
CREATE TABLE IF NOT EXISTS `music` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artiste` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_id` int(11) NOT NULL,
  `genre` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `music`
--

INSERT INTO `music` (`id`, `artiste`, `entity_id`, `genre`, `image`, `parent`, `date`, `titre`) VALUES
(1, 'Superdrag', 67913, 'a:2:{i:0;s:4:\"Rock\";i:1;s:4:\"Punk\";}', 'Superdrag-Stereo_360_Sound.jpg', 'Superdrag', '1998', 'Stereo 360 Sound'),
(2, '20syl', 67914, 'a:2:{i:0;s:7:\"Electro\";i:1;s:3:\"Pop\";}', 'Motifs.jpg', '20Syl', '2014', 'Motifs - EP'),
(3, '20syl', 67915, 'a:2:{i:0;s:7:\"Dubstep\";i:1;s:6:\"Grunge\";}', '20Sylh.jpg', '20Syl', '2015', 'Motifs II - EP'),
(4, 'C2C', 67916, 'a:2:{i:0;s:3:\"Rap\";i:1;s:7:\"Electro\";}', 'Tetra.jpg', 'C2C', '2012', 'Tetra'),
(5, 'U2', 68552, 'a:2:{i:0;s:3:\"Pop\";i:1;s:5:\"Metal\";}', 'u2.jpg', 'U2', '1996', 'sunday bloody sunday'),
(6, 'U2', 68900, 'a:2:{i:0;s:3:\"Pop\";i:1;s:5:\"Metal\";}', 'u2.jpg', 'U2', '1996', 'sunday bloody sunday');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `name`, `password`, `type`) VALUES
(1, 'Hugo', 'martin', 'admin'),
(2, 'Marine', 'marmonier', 'user'),
(3, 'Émile', 'habile', 'user'),
(4, 'William', 'Dafoe', 'admin'),
(5, 'papa', 'papa', 'admin'),
(6, 'John', 'cina', 'user'),
(7, 'whocares', 'nobody', 'admin');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
