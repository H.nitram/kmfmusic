<?php
require_once('bdd/connexion.php');
$db = get_db();
$reponse ='SELECT * FROM music';
$res = $db->query($reponse);
$output="";
//permet de genererer les "plaques" pour chaque ligne de la table music
  while($row = $res->fetch_assoc())
  {
    $array = unserialize($row['genre']);
    $img = $row['image'];
    $output .='<div class="musicase">
                  <img class="dots" src="img/dots.svg" alt="dots" title="Modif/Suppr">
                  <section class = "imgSec">
                    <input type="hidden" id="mimg" name="img" value="'.$img.'">
                    <img src="img/'.$img.'" alt="">
                  </section>
                  <section class="minfo">
                    <marquee class="mtitle"><span class="titre">'.$row['titre'].'</span> - <span class="date">'.$row['date'].'</span></marquee>
                    <p class="martist">'.$row['artiste'].'</p>';
                    $i = 1;
                    foreach ($array as $arrays => $value) {
                      $output .='<p class = "mdesc desc'.$i.'">'.$value.'</p>';
                      $i++;
                    }
    $output .='</section>
              <input type="hidden" id="mid" name="id" value="'.$row['id'].'">
              <input type="hidden" id="mentity" name="identity" value="'.$row['entity_id'].'">
              <input type="hidden" id="mparent" name="parent" value="'.$row['parent'].'">
              </div>';
  }
  echo $output;
  ?>
