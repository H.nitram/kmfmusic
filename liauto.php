<?php
require_once('bdd/connexion.php');
$db = get_db();
$reponse ='SELECT genre FROM music';
$res = $db->query($reponse);
$output="";
//incremente des li pour autant de genre trouvé
$output.="<li class='liTitle'>GENRES</li>";
while($row = $res->fetch_assoc())
{
  $array = unserialize($row['genre']);
  foreach ($array as $arrays => $value) {
    $output .='<li class = "genre">'.$value.'</p>';
  }
}
echo $output;
